/*
 * Triangle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Triangle.h"

Triangle::Triangle(double a, double b, double c) {
	setA(a);
	setB(b);
	setC(c);
}

Triangle::~Triangle() {

}

void Triangle::setA(double a){
	this->a = a;//! added this
}

void Triangle::setB(double b){
	this->b = b;//! added this
}

void Triangle::setC(double c){
	this->c = c;//! added this
}

double Triangle::calculateCircumference(){
	return a + b + c ; //! fixed
}

