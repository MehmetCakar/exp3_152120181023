/*
 * Circle.h
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */
#include <list>
#ifndef CIRCLE_H_
#define CIRCLE_H_
class Circle {

public:
	
	Circle(double);
	~Circle();// ! deleted virtual
	void setR(double);
	void setR(int); //Overloading the setR
	double getR()const;
	double calculateCircumference()const; //Objects that write constant can only call functions that write constant
	double calculateArea();
	bool operator==(const Circle l1);  
private:
	double r;
	//double PI = 3.14; 
};
#endif /* CIRCLE_H_ */
