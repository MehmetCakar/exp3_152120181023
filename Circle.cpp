/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */
#include <list>
#define PI 3.14 // noone can chance it from anywhere like that
#include "Circle.h"
Circle:: Circle(double r) {

	setR(r);
}

Circle::~Circle() {
}

void Circle::setR(double r){
	this->r = r; 
}
void Circle::setR(int r) { //Overloading the setR
	 this->r = r;
}
double Circle::getR()const{ // adding const to 
	return r;
}

double Circle::calculateCircumference()const{//Objects that write constant can only call functions that write constant
	return PI * r * 2;// ! fixed code
}

double Circle::calculateArea(){
	return PI * r * r;// ! fixed code
}

bool Circle::operator==(const Circle l1) {

	bool result;
	result = (this-> r==l1.r);
	return result;
}